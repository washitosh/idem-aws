import uuid

import pytest


async def delete_all_keys(hub, ctx, user_name):
    keys_list = await hub.exec.aws.iam.key.list(ctx, user_name=user_name)
    assert keys_list["result"], keys_list["comment"]
    for key in keys_list["ret"]:
        ret = await hub.exec.aws.iam.key.delete(
            ctx, user_name=user_name, access_key_id=key["access_key_id"]
        )
        assert ret["result"], ret["comment"]


@pytest.fixture
async def aws_iam_user(hub, ctx, aws_iam_user):
    yield aws_iam_user
    await delete_all_keys(hub, ctx, user_name=aws_iam_user["name"])


@pytest.fixture
async def aws_iam_user_keys(hub, ctx, aws_iam_user):
    user_name = aws_iam_user["name"]
    create = await hub.exec.aws.iam.key.create(ctx, user_name=user_name)
    assert create["result"], create["comment"]

    yield create["ret"]

    await delete_all_keys(hub, ctx, user_name=user_name)


@pytest.mark.asyncio
async def test_absent_happy_path(hub, ctx, aws_iam_user_keys):
    ret = await hub.states.aws.iam.key.absent(
        ctx,
        name=f"idem-test-iam-key-{str(uuid.uuid4())}",
        resource_id=f"{aws_iam_user_keys['user_name']}-{aws_iam_user_keys['access_key_id']}",
    )
    assert ret["result"], ret["comment"]
    assert len(ret["old_state"]) == 5, "All fields should be present in old"
    assert ret["old_state"]["access_key_id"] == aws_iam_user_keys["access_key_id"]
    assert ret["new_state"] is None


@pytest.mark.asyncio
async def test_present_create_inactive(hub, ctx, aws_iam_user):
    user_name = aws_iam_user["user_name"]

    ret = await hub.states.aws.iam.key.present(
        ctx,
        name=f"idem-test-iam-key-{str(uuid.uuid4())}",
        user_name=user_name,
        status="Inactive",
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] is None
    assert ret["new_state"]["status"] == "Inactive"


@pytest.mark.asyncio
async def test_present_create_second_key(hub, ctx, aws_iam_user):
    user_name = aws_iam_user["name"]

    ret1 = await hub.states.aws.iam.key.present(
        ctx,
        name=f"idem-test-iam-key-{str(uuid.uuid4())}",
        user_name=user_name,
    )
    assert ret1["result"], ret1["comment"]
    ret2 = await hub.states.aws.iam.key.present(
        ctx,
        name=f"idem-test-iam-key-{str(uuid.uuid4())}",
        user_name=user_name,
    )
    assert ret2["result"], ret2["comment"]

    assert ret1["new_state"]["access_key_id"] != ret2["new_state"]["access_key_id"]


@pytest.mark.asyncio
async def test_lifecycle_manual_operations(hub, ctx, aws_iam_user):
    # In this test we're verifying everything works with user_name and access_key_id
    #  A user may do an initial state run with any of these states.
    user_name = aws_iam_user["name"]

    # test create
    ctx["test"] = True
    ret = await hub.states.aws.iam.key.present(
        ctx,
        name=f"idem-test-iam-key-{str(uuid.uuid4())}",
        status="Active",
        user_name=user_name,
    )
    assert ret["result"], ret["comment"]
    assert ret["new_state"]["secret_access_key"] == "SECRET_ACCESS_KEY"
    assert ret["new_state"]["status"] == "Active"
    ctx["test"] = False

    # actually create
    ret = await hub.states.aws.iam.key.present(
        ctx,
        name=f"idem-test-iam-key-{str(uuid.uuid4())}",
        status="Active",
        user_name=user_name,
    )
    assert ret["result"], ret["comment"]

    assert len(ret["new_state"]["secret_access_key"]) > 10
    assert ret["new_state"]["status"] == "Active"

    # make sure the resource id was generated
    access_key_id = ret["new_state"]["access_key_id"]
    assert ret["new_state"]["resource_id"] == f"{user_name}-{access_key_id}"

    # re-run, no changes (idempotent)
    ret = await hub.states.aws.iam.key.present(
        ctx,
        name=f"idem-test-iam-key-{str(uuid.uuid4())}",
        status="Active",
        user_name=user_name,
        access_key_id=access_key_id,
    )
    assert ret["result"], ret["comment"]
    assert "secret_access_key" not in ret["new_state"]
    assert not ret["changes"]

    # change status
    ret = await hub.states.aws.iam.key.present(
        ctx,
        name=f"idem-test-iam-key-{str(uuid.uuid4())}",
        status="Inactive",
        user_name=user_name,
        access_key_id=access_key_id,
    )
    assert ret["result"], ret["comment"]
    assert "secret_access_key" not in ret["new_state"]
    assert ret["new_state"]["status"] == "Inactive"

    # delete
    ret = await hub.states.aws.iam.key.absent(
        ctx,
        name=f"idem-test-iam-key-{str(uuid.uuid4())}",
        user_name=user_name,
        access_key_id=access_key_id,
    )
    assert ret["result"], ret["comment"]
    assert ret["new_state"] is None
    assert "deleted" in str(ret["comment"])

    # re-run, no changes (idempotent)
    ret = await hub.states.aws.iam.key.absent(
        ctx,
        name=f"idem-test-iam-key-{str(uuid.uuid4())}",
        user_name=user_name,
        access_key_id=access_key_id,
    )
    assert ret["result"], ret["comment"]
    assert ret["new_state"] is None
    assert "already absent" in str(ret["comment"])


@pytest.mark.asyncio
async def test_lifecycle_describe(hub, ctx, aws_iam_user_keys):
    # here we test state functions using the resource_id from describe
    user_name = aws_iam_user_keys["user_name"]
    access_key_id = aws_iam_user_keys["access_key_id"]

    # find a key via describe
    ret = await hub.states.aws.iam.key.describe(ctx)
    access_key = None
    for name, access_key_present in ret.items():
        tmp = {}
        for row in access_key_present["aws.iam.key.present"]:
            tmp.update(row)
        access_key = tmp
        if user_name in name and access_key["access_key_id"] == access_key_id:
            break
    assert access_key

    resource_id = access_key["resource_id"]

    ret = await hub.states.aws.iam.key.present(
        ctx,
        name="idem-test-iam-key-{str(uuid.uuid4())}",
        status="Inactive",
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret["changes"]["new"]["status"] == "Inactive"

    ret = await hub.states.aws.iam.key.absent(
        ctx,
        name=f"idem-test-iam-key-{str(uuid.uuid4())}",
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert "deleted" in str(ret["comment"])

    ret = await hub.states.aws.iam.key.absent(
        ctx,
        name=f"idem-test-iam-key-{str(uuid.uuid4())}",
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert "already absent" in str(ret["comment"])
