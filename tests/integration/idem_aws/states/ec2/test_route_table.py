import copy
import os
import random
import time
import uuid

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_route_table(
    hub, ctx, aws_ec2_vpc, aws_ec2_subnet, aws_ec2_nat_gateway, aws_ec2_internet_gateway
):
    # Create route table
    route_table_temp_name = "idem-test-route-table" + str(uuid.uuid4())
    vpc_id = aws_ec2_vpc.get("VpcId")
    tags = [{"Key": "Name", "Value": route_table_temp_name}]
    associations_to_add_on_create = [
        {"GatewayId": aws_ec2_internet_gateway.get("resource_id")}
    ]

    # Idem state --test
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.ec2.route_table.present(
        test_ctx,
        name=route_table_temp_name,
        vpc_id=vpc_id,
        associations=associations_to_add_on_create,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert vpc_id == resource.get("vpc_id")
    assert route_table_temp_name == resource.get("name")
    assert tags == resource.get("tags")
    assert associations_to_add_on_create == resource.get("associations")

    # actual creation.
    ret = await hub.states.aws.ec2.route_table.present(
        ctx,
        name=route_table_temp_name,
        vpc_id=vpc_id,
        associations=associations_to_add_on_create,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert vpc_id == resource.get("vpc_id")
    assert tags == resource.get("tags")
    assert route_table_temp_name == resource.get("name")
    assert resource.get("associations")
    association_added = resource.get("associations")[0]
    resource_id = resource.get("resource_id")
    assert resource_id == association_added["RouteTableId"]
    assert aws_ec2_internet_gateway.get("resource_id") == association_added["GatewayId"]
    assert association_added["RouteTableAssociationId"]

    new_routes = resource.get("routes")

    # Idem state --test. Removing all associations with routes and tags unchanged
    ret = await hub.states.aws.ec2.route_table.present(
        test_ctx,
        name=resource_id,
        resource_id=resource_id,
        vpc_id=vpc_id,
        tags=None,
        routes=None,
        associations=[],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    new_associations = [
        new_association
        for new_association in resource.get("associations")
        if new_association.get("Main") is not True
    ]
    assert new_associations == []
    assert new_routes == resource.get("routes")
    assert resource.get("tags") == tags

    # Removing all associations with routes and tags unchanged
    ret = await hub.states.aws.ec2.route_table.present(
        ctx,
        name=resource_id,
        resource_id=resource_id,
        vpc_id=vpc_id,
        tags=None,
        routes=None,
        associations=[],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")

    new_associations = []
    if resource.get("associations"):
        new_associations = [
            new_association
            for new_association in resource.get("associations")
            if new_association.get("Main") is not True
        ]
    assert new_associations == []
    assert new_routes == resource.get("routes")
    assert resource.get("tags") == tags

    cidr_block = os.getenv("IT_TEST_EC2_ROUTE_TABLE_DESTINATION_CIDR_BLOCK")
    num: () = lambda: random.randint(0, 255)
    if cidr_block is None:
        cidr_block = f"172.31.{num()}.0/24"
    new_routes.append(
        {
            "DestinationCidrBlock": cidr_block,
            "NatGatewayId": aws_ec2_nat_gateway.get("resource_id"),
        }
    )

    # remove main route table as we cannot modify
    new_associations = []
    if resource.get("associations"):
        new_associations = [
            new_association
            for new_association in resource.get("associations")
            if new_association.get("Main") is not True
        ]
    new_associations.append({"SubnetId": aws_ec2_subnet.get("SubnetId")})
    # Test updating tags, routes and associations
    new_tags = [
        {"Key": "new-name", "Value": resource_id},
    ]

    # Idem state --test. Perform a dummy update.
    ret = await hub.states.aws.ec2.route_table.present(
        test_ctx,
        name=route_table_temp_name,
        resource_id=resource_id,
        vpc_id=vpc_id,
        tags=new_tags,
        routes=new_routes,
        associations=new_associations,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_tags == resource.get("tags")
    new_route_added = resource.get("routes")[1]
    new_association_added = [
        new_association
        for new_association in resource.get("associations")
        if new_association.get("Main") is not True and "SubnetId" in new_association
    ][0]
    assert (
        new_route_added["DestinationCidrBlock"] == new_routes[1]["DestinationCidrBlock"]
    )
    assert new_route_added["NatGatewayId"] == new_routes[1]["NatGatewayId"]
    assert new_association_added["SubnetId"] == new_associations[0]["SubnetId"]

    for i in range(5):
        # we can associate NAT gateway to route table only if its in available state.
        ret = await hub.exec.boto3.client.ec2.describe_nat_gateways(
            ctx, NatGatewayIds=[aws_ec2_nat_gateway.get("resource_id")]
        )
        state = (
            ret["ret"]["NatGateways"][0]["State"]
            if ret["result"] and ret["ret"]["NatGateways"]
            else None
        )
        if state and state.casefold() == "pending":
            time.sleep(60)
        elif state and state.casefold() == "available":
            break

    # do an actual update.
    ret = await hub.states.aws.ec2.route_table.present(
        ctx,
        name=route_table_temp_name,
        resource_id=resource_id,
        vpc_id=vpc_id,
        tags=new_tags,
        routes=new_routes,
        associations=new_associations,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_tags == resource.get("tags")
    assert len(resource.get("routes")) == 2
    new_route_added = (
        resource.get("routes")[0]
        if (resource.get("routes")[1]).get("NatGatewayId") is None
        else resource.get("routes")[1]
    )
    new_association_added = [
        new_association
        for new_association in resource.get("associations")
        if new_association.get("Main") is not True and "SubnetId" in new_association
    ][0]
    assert (
        new_route_added["DestinationCidrBlock"] == new_routes[1]["DestinationCidrBlock"]
    )
    assert new_route_added["NatGatewayId"] == new_routes[1]["NatGatewayId"]
    assert new_association_added["SubnetId"] == new_associations[0]["SubnetId"]
    assert new_association_added["RouteTableAssociationId"]
    assert new_association_added["RouteTableId"] == resource_id

    # Idem state --test. Removing routes with associations and tags unchanged
    new_routes = [new_routes[0]]
    ret = await hub.states.aws.ec2.route_table.present(
        test_ctx,
        name=route_table_temp_name,
        resource_id=resource_id,
        vpc_id=vpc_id,
        tags=None,
        routes=new_routes,
        associations=None,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_routes == resource.get("routes")
    assert resource.get("tags") == new_tags

    # Removing routes with associations and tags unchanged
    new_routes = [new_routes[0]]
    ret = await hub.states.aws.ec2.route_table.present(
        ctx,
        name=route_table_temp_name,
        resource_id=resource_id,
        vpc_id=vpc_id,
        tags=None,
        routes=new_routes,
        associations=None,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_routes == resource.get("routes")
    assert resource.get("tags") == new_tags

    # Describe route_table
    describe_ret = await hub.states.aws.ec2.route_table.describe(ctx)
    assert resource_id in describe_ret
    hub.tool.utils.verify_in_list(
        describe_ret[resource_id]["aws.ec2.route_table.present"], "tags", new_tags
    )

    # Idem state --test. Delete route table.
    ret = await hub.states.aws.ec2.route_table.absent(
        test_ctx, name=route_table_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Would delete aws.ec2.route_table '{route_table_temp_name}'" in ret["comment"]
    )

    # Delete route_table
    ret = await hub.states.aws.ec2.route_table.absent(
        ctx, name=route_table_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Deleted '{route_table_temp_name}'" in ret["comment"]

    # Trying to delete an already deleted resource. It should say resource already got deleted.
    ret = await hub.states.aws.ec2.route_table.absent(
        ctx, name=route_table_temp_name, resource_id=resource_id
    )
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.ec2.route_table", name=route_table_temp_name
        )[0]
        in ret["comment"]
    )
